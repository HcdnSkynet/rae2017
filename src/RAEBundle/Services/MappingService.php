<?php

namespace RAEBundle\Services;

use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Welcome to my service
 * we got php
 */
class MappingService{

    private $container;
    private $em;
    private $mapTipoPeriodo;
    private $mapTipoSesion;

    public function __construct(ContainerInterface $container){
        $this->container = $container;
        $this->em = $container->get('doctrine')->getManager();
        if (!$this->em) {
            $this->em = $this->container->get('doctrine')->getManager();
        }

        $this->mapTipoPeriodo = Array("e" => $this->em->getRepository('STIRBundle:TipoPeriodo')->find(0),
            "o" => $this->em->getRepository('STIRBundle:TipoPeriodo')->find(1),
            "p" => $this->em->getRepository('STIRBundle:TipoPeriodo')->find(2));

        $this->mapTipoSesion = Array("e" => $this->em->getRepository('STIRBundle:TipoSesion')->find(0),
            "i" => $this->em->getRepository('STIRBundle:TipoSesion')->find(1),
            "p" => $this->em->getRepository('STIRBundle:TipoSesion')->find(2),
            "t" => $this->em->getRepository('STIRBundle:TipoSesion')->find(3),
            "h" => $this->em->getRepository('STIRBundle:TipoSesion')->find(4));
    }

    public function getTipoPeriodo($tipoPeriodo){
        return $this->mapTipoPeriodo[$tipoPeriodo];
    }

    public function getTipoSesion($tipoSesion){
        return $this->mapTipoSesion[$tipoSesion];
    }



}