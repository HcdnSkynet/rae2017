<?php

namespace RAEBundle\Services;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;
use STIRBundle\Entity\Acta;
use STIRBundle\Entity\DetalleActa;

class HelperService
{
    private $container;
    private $em;

    public function __construct(ContainerInterface $container){
        $this->container = $container;
        $this->em = $container->get('doctrine')->getManager();
        if (!$this->em) {
            $this->em = $this->container->get('doctrine')->getManager();
        }
    }

    public function getProximoCodigoPersona(){

        $generados = $this->container->get('doctrine')->getManager()->createQuery(
            "SELECT d FROM STIRBundle:Diputado d 
               WHERE d.codigo like 'G%' ")->getResult();
        return "G".sizeof($generados);

    }

    public function getActasPendientes(){

        $pendientes = $this->container->get('doctrine')->getManager()->createQuery(
            "SELECT a 
               FROM STIRBundle:Acta a 
               INNER JOIN STIRBundle:Reunion r WHERE a.reunion = r
               INNER JOIN STIRBundle:Periodo p WHERE r.periodo = p
               WHERE SIZE(a.detalleActas) < 257 AND a.tipoVotacion = 2
               ORDER BY p.id DESC, r.reunion DESC,  a.numero DESC ")
            ->getResult();

        return $pendientes;
    }

    public function getActasFinalizadas(){

        $pendientes = $this->container->get('doctrine')->getManager()->createQuery(
            "SELECT a , r 
               FROM STIRBundle:Acta a 
               INNER JOIN STIRBundle:Reunion r WHERE a.reunion = r
               INNER JOIN STIRBundle:Periodo p WHERE r.periodo = p
               WHERE SIZE(a.detalleActas) = 257
               ORDER BY p.id ASC, r.reunion ASC,  a.numero ASC ")
            ->getResult();

        return $pendientes;
    }

    public function copyDetalles(Acta $modeloActa,Acta $actaSinDetalle){
        if(sizeof($actaSinDetalle->getDetalleActas())>0){
            die("Error con acta, peligro de duplicacion " . $actaSinDetalle->getId());
        }

        $detalleOrdenado = $this->em->createQuery(
            "SELECT d 
               FROM STIRBundle:DetalleActa d 
               INNER JOIN STIRBundle:Voto v WHERE d.voto = v
               LEFT JOIN STIRBundle:Diputado di WHERE d.diputado = di
               LEFT JOIN STIRBundle:Bloque b    WHERE d.bloque = b
               WHERE d.acta = :acta
               ORDER BY v.voto ASC , di.apellido ASC")
            ->setParameter("acta",$modeloActa)
            ->getResult();

        $tSizeDetalleAntes = sizeof($actaSinDetalle->getDetalleActas());
        $tSizeDetalleModelo = sizeof($detalleOrdenado);

        $insertados = 0;
        $mismoPresidente = false;
        if($actaSinDetalle->getPresidente()->getId()==$modeloActa->getPresidente()->getId()){
            $mismoPresidente = true;
        }

        $this->em = $this->container->get('doctrine')->getManager();

        $eAusente = $this->em->getRepository("STIRBundle:Voto")->getAusente();

        foreach( $detalleOrdenado as $detalle ){
            $nuevoDetalle = new DetalleActa();
            $nuevoDetalle->setBloque($detalle->getBloque());
            $nuevoDetalle->setProvincia($detalle->getProvincia());
            $nuevoDetalle->setActa($actaSinDetalle);
            if($detalle->getDiputado!=null){
                if($detalle->getDiputado->getId()==$actaSinDetalle->getPresidente()->getId()){
                    $nuevoDetalle->setVoto($eAusente);
                }
            }else{
                $nuevoDetalle->setVoto($detalle->getVoto());
            }
            $nuevoDetalle->setDiputado($detalle->getDiputado());
            $nuevoDetalle->setVideoEnd($detalle->getVideoEnd());
            $nuevoDetalle->setVideoStart($detalle->getVideoStart());
            $nuevoDetalle->setVideoId($detalle->getVideoId());
            $actaSinDetalle->addDetalleActa($nuevoDetalle);
            $this->container->get('doctrine')->getManager()->persist($nuevoDetalle);
            $insertados++;
        }

        if($insertados!=257){
            die("No se pudo copiar el acta correctamente. Acta modelo: " . $modeloActa->getId() . " | A insertar: " . $insertados);
        }

        /* Inserto al presidente */

        /* Obtengo voto AUSENTE */

        /*$this->em = $this->container->get('doctrine')->getManager();

        $eAusente = $this->em->getRepository("STIRBundle:Voto")->getAusente();

        $dPresidente = new DetalleActa();
        $dPresidente->setBloque($actaSinDetalle->getPresidente()->getBloque());
        $dPresidente->setProvincia($actaSinDetalle->getPresidente()->getProvincia());
        $dPresidente->setActa($actaSinDetalle);
        $dPresidente->setVoto($eAusente);
        $dPresidente->setDiputado($actaSinDetalle->getPresidente());
        $dPresidente->setVideoEnd("");
        $dPresidente->setVideoStart("");
        $dPresidente->setVideoId("");

        $this->em->persist($dPresidente);

        $actaSinDetalle->addDetalleActa($dPresidente);*/

        $this->container->get('doctrine')->getManager()->flush();

        $tSizeDetalleDespues = sizeof($actaSinDetalle->getDetalleActas());

        if(sizeof($tSizeDetalleDespues)>257){
            print("ERROR. Acta Modelo: " . $modeloActa->getId() . "|" . " Acta Sin Detalle: " . $actaSinDetalle->getId() . "<br>");
            die("ERROR. Detalle Antes: " . $tSizeDetalleAntes . "|" . " Detalle Modelo: " . $tSizeDetalleModelo . "|" . " Detalle Despues " . $tSizeDetalleDespues);
        }

        return $actaSinDetalle->getDetalleActas();
    }

    public function actaTieneDetalle($acta){

        return ( sizeof($acta->getDetalleActas())>0 ) ? true : false;

    }

    public function getDetalleOrdenado($acta, $order){
        if($order==1){
            return $this->container->get('doctrine')->getManager()->createQuery(
                "SELECT d 
               FROM STIRBundle:DetalleActa d 
               LEFT JOIN STIRBundle:Voto v WHERE d.voto = v
               LEFT JOIN STIRBundle:Diputado di WHERE d.diputado = di
               LEFT JOIN STIRBundle:Bloque b    WHERE d.bloque = b
               WHERE d.acta = :acta
               ORDER BY v.voto ASC , di.apellido ASC, di.nombre ASC")
                ->setParameter("acta",$acta)
                ->getResult();
        }
        return $this->container->get('doctrine')->getManager()->createQuery(
            "SELECT d 
               FROM STIRBundle:DetalleActa d 
               LEFT JOIN STIRBundle:Voto v WHERE d.voto = v
               LEFT JOIN STIRBundle:Diputado di WHERE d.diputado = di
               LEFT JOIN STIRBundle:Bloque b    WHERE d.bloque = b
               WHERE d.acta = :acta
               ORDER BY di.apellido ASC, di.nombre ASC")
            ->setParameter("acta",$acta)
            ->getResult();
    }

    public function getBloquesSinIdsInvalidos(){

        return $this->container->get('doctrine')->getManager()->createQuery(
            "SELECT b 
               FROM STIRBundle:Bloque b 
               WHERE b.id != 4000 AND b.id != 266 AND b.id != 314 AND b.id != 2024 
               ORDER BY b.nombre ASC")
            ->getResult();

    }

    public function getDiputadosNotInActa($acta){

        $diputadosEnActa = $this->container->get('doctrine')->getManager()->createQuery(
            "SELECT di.id 
               FROM STIRBundle:DetalleActa d 
               INNER JOIN STIRBundle:Diputado di WHERE d.diputado = di
               WHERE d.acta = :acta
               ORDER BY di.apellido ASC")
            ->setParameter("acta",$acta)
            ->getResult();

        $faltantes = $this->container->get('doctrine')->getManager()->createQuery(
            "SELECT d FROM STIRBundle:Diputado d 
               WHERE d.id not in (:diputadosEnActa)
               ORDER BY d.apellido ASC")
            ->setParameter('diputadosEnActa',$diputadosEnActa)
            ->getResult();

        return $faltantes;
    }

    public function getDiputadosActivos()
    {

        $diputadosActivos = $this->container->get('doctrine')->getManager()->createQuery(
            "SELECT d FROM STIRBundle:Diputado d INNER JOIN STIRBundle:Estado e WHERE d.estado = e
               WHERE e.id <= 2 ORDER BY d.apellido, d.nombre")->getResult();
        return $diputadosActivos;
    }

    public function getDetalleActaNueva(Request $request, Acta $actaNueva)
    {
        $periodo = $request->get('periodo');
        $numeroReunion = $request->get('reunion');
        $numeroActa = $request->get('acta');

        $periodo = $this->em->getRepository('STIRBundle:Periodo')->find($periodo);

        $reunionBuscada = $this->em->getRepository('STIRBundle:Reunion')->findOneBy(array("periodo" => $periodo,
            "reunion" => $numeroReunion));

        $actaModeloBuscada = $this->em->getRepository('STIRBundle:Acta')->findOneBy(array("reunion" => $reunionBuscada,
            "numero" => $numeroActa));

        if(!$actaModeloBuscada){
            throw new \Exception("El acta modelo seleccionada no existe");
        }

        if (sizeof($actaModeloBuscada->getDetalleActas()) != 257) {
            die("El acta modelo tiene un detalle erroneo de tamanio diferente a 257: " . sizeof($actaModeloBuscada->getDetalleActas()) . " | ID " . $actaModeloBuscada->getId());
        }

        if (!$periodo) {
            throw new \Exception("Periodo no existente");
        }
        if (!$reunionBuscada) {
            throw new \Exception("Reunion no existente");
        }
        if (!$actaModeloBuscada) {
            throw new \Exception("Acta no existente");
        }

        $detalles = $this->copyDetalles($actaModeloBuscada, $actaNueva);

        return $detalles;
    }

    public function getLastIdMySQLBloque(){

        $idNuevoBloque = $this->em->createQueryBuilder()
            ->select('MAX(b.id)')
            ->from('STIRBundle:Bloque', 'b')
            ->where('b.id < 2000')
            ->getQuery()
            ->getSingleScalarResult();

        return $idNuevoBloque+1;

    }

    public function getActasConDetalle(){
        $actasConDetalle = $this->em->createQuery(
            "SELECT a 
               FROM STIRBundle:Acta a 
               INNER JOIN STIRBundle:Reunion r WHERE a.reunion = r
               INNER JOIN STIRBundle:Periodo p WHERE r.periodo = p
               WHERE SIZE(a.detalleActas) = 257 AND a.tipoVotacion = 2
               ORDER BY p.id DESC, r.reunion ASC,  a.numero ASC ")
            ->getResult();

        return $actasConDetalle;
    }



}