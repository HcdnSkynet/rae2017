<?php

namespace RAEBundle\Services;

use STIRBundle\Entity\Acta;
use STIRBundle\Entity\DetalleActa;
use STIRBundle\Entity\Bloque;
use STIRBundle\Entity\Diputado;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;
use DateTime;
use STIRBundle\Entity\Periodo;
use STIRBundle\Entity\Reunion;

class RequestHandlerService
{

    private $container;
    private $em;

    public function __construct(ContainerInterface $container){
        $this->container = $container;
        $this->em = $container->get('doctrine')->getManager();
        if (!$this->em) {
            $this->em = $this->container->get('doctrine')->getManager();
        }
    }

    public function editarActa(Request $request){

        //obtengo y formateo los datos del form
        $periodoRaw = $request->get('periodo-raw');
        $periodo = $this->em->getRepository('STIRBundle:Periodo')->find(substr($periodoRaw, 0, 3));

        if (!$periodo) {
            throw new \Exception("El periodo no existe");
        }

        $reunion = $request->get('reunion-edicion');

        $formatoFechaYHora = 'd/m/Y H:i';
        $fechaYHora = DateTime::createFromFormat($formatoFechaYHora, $request->get('fecha-acta') . " " . $request->get('hora-acta'));

        $baseMayoria = $this->em->getRepository('STIRBundle:BaseMayoria')->find($request->get('base-mayoria'));
        $tipoVotacion = $this->em->getRepository('STIRBundle:TipoVotacion')->find($request->get('tipo-votacion'));
        $tipoMayoria = $this->em->getRepository('STIRBundle:TipoMayoria')->find($request->get('tipo-mayoria'));
        $resultadoVotacion = $this->em->getRepository('STIRBundle:Resultado')->find($request->get('resultado-votacion'));
        $votoPresidente = $this->em->getRepository('STIRBundle:Voto')->find($request->get('voto-presidente'));
        if (!$votoPresidente) {
            $votoPresidente = null;
        }
        $idPresidente = $request->get('presidente');
        $numeroActa = $request->get('numero-acta-edicion');
        $votosAfirmativos = $request->get('votos-afirmativos');
        $votosNegativos = $request->get('votos-negativos');
        $abstenciones = $request->get('abstenciones');
        $ausentes = $request->get('ausentes');
        $tituloActa = $request->get('titulo-acta');
        $tituloPublicoActa = $request->get('titulo-publico-acta');
        $observacionActa = $request->get('observacion-acta');
        $presentesSinIdentificar = $request->get('presentes-sin-identificar');

        //Si no existe la reunion, la creo, si existe creo el acta y se la asigno
        $reunionBuscada = $this->em->getRepository('STIRBundle:Reunion')->findOneBy(array("periodo" => $periodo,
            "reunion" => $reunion));

        if (!$reunionBuscada) {
            throw new \Exception("La reunion no existe");
        }

        $actaBuscada = $this->em->getRepository('STIRBundle:Acta')->findOneBy(Array("reunion" => $reunionBuscada,
            "numero" => $numeroActa));

        if (!$actaBuscada) {
            throw new Exception("El acta no existe.");
        }


        $actaBuscada = $this->updateActa($actaBuscada, $fechaYHora, $abstenciones, $votosAfirmativos, $ausentes, $votosNegativos, $presentesSinIdentificar, $baseMayoria, $observacionActa, $idPresidente, $resultadoVotacion, $tipoMayoria, $tipoVotacion, $tituloActa, $tituloPublicoActa, $votoPresidente);

        $this->em->persist($actaBuscada);
        $this->em->flush();

        return "Modificada el acta numero " . $actaBuscada->getNumero() . "\n";

    }

    public function crearActa(Request $request){

        //obtengo y formateo los datos del form
        $periodoRaw = $request->get('periodo-raw');
        $periodo = $this->em->getRepository('STIRBundle:Periodo')->find(substr($periodoRaw, 0, 3));

        //si no hay periodo lo creo
        if (!$periodo) {

            $periodo = new Periodo();
            $periodo->setId(substr($periodoRaw, 0, 3));
            $periodo->setFechaInicio(new DateTime());
            $periodo->setFechaFin(new DateTime());

            $this->em->persist($periodo);

            $mensaje = "Creado periodo " . $periodo->getId() . "\n";
        }

        $tipoSesion = $this->container->get('mapping')->getTipoSesion(strtolower(substr($periodoRaw, 4, 1)));
        $tipoPeriodo = $this->container->get('mapping')->getTipoPeriodo(strtolower(substr($periodoRaw, 3, 1)));

        $sesion = $request->get('sesion');
        $reunion = $request->get('reunion');

        $formatoFecha = 'd/m/Y';
        $fecha = DateTime::createFromFormat($formatoFecha, $request->get('fecha-acta'));

        $formatoFechaYHora = 'd/m/Y H:i';
        $fechaYHora = DateTime::createFromFormat($formatoFechaYHora, $request->get('fecha-acta') . " " . $request->get('hora-acta'));

        $baseMayoria = $this->em->getRepository('STIRBundle:BaseMayoria')->find($request->get('base-mayoria'));
        $tipoVotacion = $this->em->getRepository('STIRBundle:TipoVotacion')->find($request->get('tipo-votacion'));
        $tipoMayoria = $this->em->getRepository('STIRBundle:TipoMayoria')->find($request->get('tipo-mayoria'));
        $resultadoVotacion = $this->em->getRepository('STIRBundle:Resultado')->find($request->get('resultado-votacion'));
        $votoPresidente = null;
        if ($request->get('voto-presidente') != "null") {
            $votoPresidente = $this->em->getRepository('STIRBundle:Voto')->find($request->get('voto-presidente'));
        }
        $idPresidente = $request->get('presidente');
        $numeroActa = $request->get('numero-acta');
        $votosAfirmativos = $request->get('votos-afirmativos');
        $votosNegativos = $request->get('votos-negativos');
        $abstenciones = $request->get('abstenciones');
        $ausentes = $request->get('ausentes');
        $tituloActa = $request->get('titulo-acta');
        $tituloPublicoActa = $request->get('titulo-publico-acta');
        $observacionActa = $request->get('observacion-acta');
        $presentesSinIdentificar = $request->get('presentes-sin-identificar');

        //Si no existe la reunion, la creo, si existe creo el acta y se la asigno
        $reunionBuscada = $this->em->getRepository('STIRBundle:Reunion')->findOneBy(array("periodo" => $periodo,
            "reunion" => $reunion,
            "sesion" => $sesion));

        if (!$reunionBuscada) {
            $reunionBuscada = $this->createReunion($periodo, $tipoPeriodo, $reunion, $sesion, $tipoSesion, $fecha);
            $this->em->persist($reunionBuscada);
            $mensaje = $mensaje . "Creada reunion " . $reunionBuscada->getReunion() . "\n";
        }

        $actaBuscada = $this->em->getRepository('STIRBundle:Acta')->findOneBy(Array("reunion" => $reunionBuscada,
            "numero" => $numeroActa));

        if ($actaBuscada) {
            throw new Exception("El acta ya existe.");
        }

        $nuevaActa = $this->createActa($reunionBuscada, $fechaYHora, $abstenciones, $votosAfirmativos, $ausentes, $votosNegativos, $presentesSinIdentificar, $baseMayoria, $numeroActa, $observacionActa, $periodoRaw, $idPresidente, $resultadoVotacion, $tipoMayoria, $tipoVotacion, $tituloActa, $tituloPublicoActa, $votoPresidente);
        $this->em->persist($nuevaActa);
        $this->em->flush();

        $mensaje = $mensaje . "Creada el acta numero " . $nuevaActa->getNumero() . "\n";
        $mensaje = "Acta creada exitosamente.";
        return $mensaje;

    }

    public function crearDiputadoYOBloque(Request $request){

        $apellidoDiputado = $request->get('apellido');
        $nombreDiputado = $request->get('nombre');
        $idBloque = $request->get('idBloque');
        $idProvincia = $request->get('idProvincia');
        $nombreNuevoBloque = $request->get('idBloqueNuevo');
        $sexo = $request->get('sexo');

        $nuevoDiputado = new Diputado();
        $nuevoDiputado->setApellido($apellidoDiputado);
        $nuevoDiputado->setNombre($nombreDiputado);
        $nuevoDiputado->setProvincia($this->em->getRepository('STIRBundle:Provincia')->find($idProvincia));
        $nuevoDiputado->setEstado($this->em->getRepository('STIRBundle:Estado')->find(7));
        $nuevoDiputado->setCuil("");
        $nuevoDiputado->setBanca(-1);
        $nuevoDiputado->setCodigoSeguridad(0);
        $nuevoDiputado->setCodigo($this->container->get('helper')->getProximoCodigoPersona());
        $nuevoDiputado->setSexo($sexo);

        //Si el bloque existe se lo seteo, si no existe lo creo y luego se lo seteo
        if ($idBloque != -1) {
            $nuevoDiputado->setBloque($this->em->getRepository('STIRBundle:Bloque')->find($idBloque));
        } else {
            $nuevoBloque = new Bloque();
            $nuevoBloque->setId($this->container->get('helper')->getLastIdMySQLBloque());
            $nuevoBloque->setNombre($nombreNuevoBloque);
            $nuevoBloque->setInterbloque(null);
            $this->em->persist($nuevoBloque);
            $this->em->flush();
            $nuevoDiputado->setBloque($nuevoBloque);
        }

        $this->em->persist($nuevoDiputado);
        $this->em->flush();

        return $nuevoDiputado;
    }

    public function updateDetalleActa(Request $request){

        $idDetalle = $request->get('idDetalle');
        $idDiputado = $request->get('idDiputado');
        $idBloque = $request->get('idBloque');
        $idProvincia = $request->get('idProvincia');


        $detalleActa = $this->em->getRepository('STIRBundle:DetalleActa')->find($idDetalle);

        if (!$detalleActa) {
            throw new \Exception("Detalle no existente");
        }

        $diputado = $this->em->getRepository('STIRBundle:Diputado')->find($idDiputado);
        $bloque = $this->em->getRepository('STIRBundle:Bloque')->find($idBloque);
        $provincia = $this->em->getRepository('STIRBundle:Provincia')->find($idProvincia);
        $detalleActa->setDiputado($diputado);
        $detalleActa->setBloque($bloque);
        $detalleActa->setProvincia($provincia);

        $this->em->persist($detalleActa);
        $this->em->flush();

        return $detalleActa;
    }

    public function updateAllVotos(Request $request){

        $idVoto = $request->get('idVoto');
        $idActa = $request->get('idActa');

        $voto = $this->em->getRepository('STIRBundle:Voto')->find($idVoto);
        $acta = $this->em->getRepository('STIRBundle:Acta')->find($idActa);

        if(!$voto || !$acta){
            throw new Exception("No se pudo procesar el pedido.");
        }

        //recorro todos los detalles y los actualizo, a menos que sea el presidente que se mantiene como ausente siempre
        foreach ($acta->getDetalleActas() as $detalle) {
            if ($detalle->getDiputado()->getId() != $acta->getPresidente()->getId()){
                $detalle->setVoto($voto);
            }
        }

        $this->em->persist($acta);
        $this->em->flush();

        return $voto;
    }

    private function updateActa($actaBuscada, $fechaYHora, $abstenciones, $votosAfirmativos, $ausentes, $votosNegativos, $presentesSinIdentificar, $baseMayoria, $observacionActa, $idPresidente, $resultadoVotacion, $tipoMayoria, $tipoVotacion, $tituloActa, $tituloPublicoActa, $votoPresidente)
    {

        $actaBuscada->setFecha($fechaYHora);
        $actaBuscada->setAbstenciones($abstenciones);
        $actaBuscada->setAfirmativos($votosAfirmativos);
        $actaBuscada->setAusentes($ausentes + $presentesSinIdentificar);
        $actaBuscada->setNegativos($votosNegativos);
        $actaBuscada->setBaseMayoria($baseMayoria);
        $actaBuscada->setPresentesNoIdentificables($presentesSinIdentificar);

        $actaBuscada->setDesempate(0);
        $actaBuscada->setObservaciones($observacionActa);
        $actaBuscada->setPresidente($this->em->getRepository('STIRBundle:Diputado')->find($idPresidente));
        $actaBuscada->setResultado($resultadoVotacion);
        $actaBuscada->setTipoMayoria($tipoMayoria);
        $actaBuscada->setTipoVotacion($tipoVotacion);
        $actaBuscada->setTitulo($tituloActa);
        $actaBuscada->setTituloPublico("");
        $actaBuscada->setVotoPresidente($votoPresidente);

        return $actaBuscada;
    }

    private function createActa($reunionBuscada, $fechaYHora, $abstenciones, $votosAfirmativos, $ausentes, $votosNegativos, $presentesSinIdentificar, $baseMayoria, $numeroActa, $observacionActa, $periodoRaw, $idPresidente, $resultadoVotacion, $tipoMayoria, $tipoVotacion, $tituloActa, $tituloPublicoActa, $votoPresidente)
    {
        $nuevaActa = new Acta();
        $nuevaActa->setReunion($reunionBuscada);
        $nuevaActa->setFecha($fechaYHora);
        $nuevaActa->setAbstenciones($abstenciones);
        $nuevaActa->setAfirmativos($votosAfirmativos);
        $nuevaActa->setAusentes($ausentes + $presentesSinIdentificar);
        $nuevaActa->setNegativos($votosNegativos);
        $nuevaActa->setBaseMayoria($baseMayoria);

        $nuevaActa->setEstado(0);
        $nuevaActa->setPresentesNoIdentificables($presentesSinIdentificar);

        $nuevaActa->setBloqueado(0);
        $nuevaActa->setDesempate(0);
        $nuevaActa->setLink("");
        $nuevaActa->setNumero($numeroActa);
        $nuevaActa->setObservaciones($observacionActa);
        $nuevaActa->setPeriodoRaw($periodoRaw);
        $nuevaActa->setPresidente($this->em->getRepository('STIRBundle:Diputado')->find($idPresidente));
        $nuevaActa->setResultado($resultadoVotacion);
        $nuevaActa->setTipoMayoria($tipoMayoria);
        $nuevaActa->setTipoVotacion($tipoVotacion);
        $nuevaActa->setTitulo($tituloActa);
        $nuevaActa->setTituloPublico("");
        $nuevaActa->setVersion(0);
        $nuevaActa->setVideoStart("");
        $nuevaActa->setVideoId("");
        $nuevaActa->setVideoEnd("");
        $nuevaActa->setVotoPresidente($votoPresidente);
        $nuevaActa->setVideoVotacion("");
        return $nuevaActa;
    }

    private function createReunion($periodo, $tipoPeriodo, $reunion, $sesion, $tipoSesion, $fecha)
    {
        $nuevaReunion = new Reunion();
        $nuevaReunion->setPeriodo($periodo);
        $nuevaReunion->setTipoPeriodo($tipoPeriodo);
        $nuevaReunion->setReunion($reunion);
        $nuevaReunion->setSesion($sesion);
        $nuevaReunion->setTipoSesion($tipoSesion);
        $nuevaReunion->setFecha($fecha);
        $nuevaReunion->setPlaylist("");
        return $nuevaReunion;
    }



}