<?php

namespace RAEBundle\Controller;

use RAEBundle\Services\MappingService;
use STIRBundle\Entity\Bloque;
use STIRBundle\Entity\Diputado;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Exception;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Validator\Constraints\DateTime;

class AjaxController extends Controller
{
    /**
     * @Route("/ajax/select/acta", name="_ajax_select_acta")
     * @param Request $request
     * @return JsonResponse
     */
    public function selectActaAction(Request $request)
    {

        $em = $this->getDoctrine()->getManager();
        $result = true;

        $periodo = $request->get('periodo');
        $numeroReunion = $request->get('reunion');
        $numeroActa = $request->get('acta');

        try {

            $periodo = $em->getRepository('STIRBundle:Periodo')->find($periodo);
            //$tipoSesion = $this->get('mapping')->getTipoSesion(strtolower(substr($periodoRaw, 4, 1)));
            //$tipoPeriodo = $this->get('mapping')->getTipoPeriodo(strtolower(substr($periodoRaw, 3, 1)));

            $reunionBuscada = $em->getRepository('STIRBundle:Reunion')->findOneBy(array("periodo" => $periodo,
                                                                                        "reunion" => $numeroReunion));

            $actaBuscada = $em->getRepository('STIRBundle:Acta')->findOneBy(array("reunion" => $reunionBuscada,
                                                                                    "numero" => $numeroActa));

            if (!$periodo) {
               throw new \Exception("Periodo no existente");
            }
            if (!$reunionBuscada) {
                throw new \Exception("Reunion no existente");
            }
            if (!$actaBuscada) {
                throw new \Exception("Acta no existente");
            }

        } catch (\Exception $e) {
            return new JsonResponse(array("success" => false,"message"=>$e->getMessage()));
        }

        //si no hay errores parseo el acta y la envio
        $actaParseada = $this->parsearActa($actaBuscada);

        return new JsonResponse(array("success" => $result, "acta" => $actaParseada));
    }

    /**
     * @Route("/ajax/update/acta/detalle/", name="_ajax_modificar_detalle")
     * @param Request $request
     * @return JsonResponse
     */
    public function updateDetalleActaAction(Request $request)
    {

        $em = $this->getDoctrine()->getManager();
        $result = true;

        try {

            $detalleActa = $this->get('request_handler')->updateDetalleActa($request);

        } catch (\Exception $e) {
            return new JsonResponse(array("success" => false,"message"=>$e->getMessage()));
        }

        return new JsonResponse(array("success" => $result,
                                        "diputado" => $detalleActa->getDiputado(),
                                        //"votoId"=>$detalleActa->getVoto()->getId(),
                                        //"votoNombre"=>$detalleActa->getVoto()->getVoto(),
                                        "bloque"=>$detalleActa->getBloque(),
                                        "provincia"=>$detalleActa->getProvincia(),
                                        "diputados"=>$this->get('helper')->getDiputadosNotInActa($detalleActa->getActa())));

    }

    /**
     * @Route("/ajax/update/acta/detalle/voto", name="_ajax_modificar_detalle_voto")
     * @param Request $request
     * @return JsonResponse
     */
    public function updateDetalleActaVotoAction(Request $request)
    {

        $em = $this->getDoctrine()->getManager();
        $result = true;

        $idDetalle = $request->get('idDetalle');
        $idVoto = $request->get('idVoto');

        try {

            $detalleActa = $em->getRepository('STIRBundle:DetalleActa')->find($idDetalle);

            if (!$detalleActa) {
                throw new \Exception("Detalle no existente");
            }

        } catch (\Exception $e) {
            return new JsonResponse(array("success" => false,"message"=>$e->getMessage()));
        }

        $voto = $em->getRepository('STIRBundle:Voto')->find($idVoto);
        $detalleActa->setVoto($voto);

        $em->persist($detalleActa);
        $em->flush();

        return new JsonResponse(array("success" => $result,
                                        "votoId"=>$detalleActa->getVoto()->getId(),
                                        "votoNombre"=>$detalleActa->getVoto()->getVoto(),
                                        "idDetalleModificado"=>$idDetalle));

    }

    /**
     * @Route("/ajax/select/acta/detalles", name="_ajax_select_detalle_acta")
     * @param Request $request
     * @return JsonResponse
     */
    public function selectDetalleActaAction(Request $request)
    {

        $em = $this->getDoctrine()->getManager();
        $result = true;

        $periodo = $request->get('periodo');
        $numeroReunion = $request->get('reunion');
        $numeroActa = $request->get('acta');

        try {

            $periodo = $em->getRepository('STIRBundle:Periodo')->find($periodo);

            $reunionBuscada = $em->getRepository('STIRBundle:Reunion')->findOneBy(array("periodo" => $periodo,
                "reunion" => $numeroReunion));

            $actaBuscada = $em->getRepository('STIRBundle:Acta')->findOneBy(array("reunion" => $reunionBuscada,
                "numero" => $numeroActa));

            if (!$periodo) {
                throw new \Exception("Periodo no existente");
            }
            if (!$reunionBuscada) {
                throw new \Exception("Reunion no existente");
            }
            if (!$actaBuscada) {
                throw new \Exception("Acta no existente");
            }

        } catch (\Exception $e) {
            return new JsonResponse(array("success" => false,"message"=>$e->getMessage()));
        }

        //si no hay errores consigo el detalle y lo envio
        $detalleActa = $actaBuscada->getDetalleActas();

        return new JsonResponse(array("success" => $result, "detalleActas" => $detalleActa));
    }

    /**
     * @Route("/create/diputado", name="_ajax_create_diputado")
     * @param Request $request
     * @return JsonResponse
     */
    public function createDiputadoAction(Request $request){

        $em = $this->getDoctrine()->getManager();
        $result = true;
        $message = "Diputado creado correctamente";

        try{
            $nuevoDiputado = $this->get('request_handler')->crearDiputadoYOBloque($request);
        }catch (Exception $e){
            $message = $e->getMessage();
            $result = false;
            $nuevoDiputado = null;
        }

        //si no hay errores envio el mensaje de creado exitosamente
        return new JsonResponse(array("success" => $result,
                                        "message" => $message,
                                        "nuevoDiputado" =>$nuevoDiputado,
                                        "nuevaListaDiputados" =>$em->getRepository('STIRBundle:Diputado')->findAll(),
                                        "nuevaListaBloques"=>$em->getRepository('STIRBundle:Bloque')->findAll()
                                        ));

    }

    /**
     * @Route("/update/acta/all/votos", name="_update_acta_all_votos")
     * @param Request $request
     * @return JsonResponse
     */
    public function updateActaAllVotosAction(Request $request){

        $result = true;
        $message = "Votos actualizados correctamente";

        try{
            $voto = $this->get('request_handler')->updateAllVotos($request);
        }catch (Exception $e){
            $message = $e->getMessage();
            $result = false;
        }

        return new JsonResponse(array("success" => $result,
            "message" => $message,
        ));

    }

    private function parsearActa($actaBuscada){

        $votoPresidente = ($actaBuscada->getVotoPresidente() == null? null : $actaBuscada->getVotoPresidente()->getId());

        $actaParseada = Array(
            "baseMayoria" => $actaBuscada->getBaseMayoria()->getId(),
            "tipoVotacion" => $actaBuscada->getTipoVotacion()->getId(),
            "tipoMayoria" => $actaBuscada->getTipoMayoria()->getId(),
            "resultado" => $actaBuscada->getResultado()->getId(),
            "votoPresidente" => $votoPresidente,
            "presidente" => $actaBuscada->getPresidente()->getId(),
            "numeroActa" => $actaBuscada->getNumero(),
            "numeroReunion" => $actaBuscada->getReunion()->getReunion(),
            "fecha" => $actaBuscada->getFecha()->format('d-m-Y'),
            "hora"=> $actaBuscada->getFecha()->format('H:i'),
            "afirmativos" => $actaBuscada->getAfirmativosEstatico(),
            "negativos" => $actaBuscada->getNegativosEstatico(),
            "ausentes" => ($actaBuscada->getAusentesEstatico() - $actaBuscada->getPresentesNoIdentificables()),
            "abstenciones" => $actaBuscada->getAbstencionesEstatico(),
            "presentesSinIdentificar" => $actaBuscada->getPresentesNoIdentificables(),
            "titulo" => $actaBuscada->getTitulo(),
            "tituloPublico" => $actaBuscada->getTituloPublico(),
            "observaciones" => $actaBuscada->getObservaciones(),
            "periodoRaw"=>$actaBuscada->getPeriodoRaw()
        );

        return $actaParseada;
    }


}
