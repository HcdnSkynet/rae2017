<?php

namespace RAEBundle\Controller;

use Exception;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RedirectResponse;

class DefaultController extends Controller{
    /**
     * @Route("/create/acta", name="_create_acta")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function createActaAction(Request $request){

        $mensaje = null;
        $error = null;
        $em = $this->container->get('doctrine')->getManager();

        if ($request->getMethod() == 'POST') {
            try {
                $mensaje = $this->get('request_handler')->crearActa($request);
            } catch (Exception $e) {
                $error = "Ocurrio un error creando el acta : " . $e->getMessage();
            }
        }

        //datos para renderizar la pagina
        $baseMayorias = $em->getRepository('STIRBundle:BaseMayoria')->findAll();
        $tiposMayoria = $em->getRepository('STIRBundle:TipoMayoria')->findAll();
        $tiposResultados = $em->getRepository('STIRBundle:Resultado')->findAll();
        $tiposVotacion = $em->getRepository('STIRBundle:TipoVotacion')->findAll();
        $votosPosibles = $em->getRepository('STIRBundle:Voto')->findAll();
        $diputados = $em->getRepository('STIRBundle:Diputado')->findBy([], ['apellido' => 'ASC']);
        $bloques = $em->getRepository('STIRBundle:Bloque')->findAll();
        $provincias = $em->getRepository('STIRBundle:Provincia')->findAll();

        return $this->render('RAEBundle:Default:crear_acta.html.twig', Array("baseMayoria" => $baseMayorias,
            "tipoMayoria" => $tiposMayoria,
            "tipoResultado" => $tiposResultados,
            "tiposVotacion" => $tiposVotacion,
            "votosPosibles" => $votosPosibles,
            "diputadosActivos" => $diputados,
            "bloques" => $bloques,
            "provincias" => $provincias,
            "mensaje" => $mensaje,
            "error" => $error));
    }

    /**
     * @Route("/update/acta", name="_update_acta")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function editarActaAction(Request $request)
    {

        $mensaje = null;
        $error = null;
        $em = $this->container->get('doctrine')->getManager();

        if ($request->getMethod() == 'POST') {
            try {
                $mensaje = $this->get('request_handler')->editarActa($request);
            } catch (Exception $e) {
                $error = "Ocurrio un error creando el acta : " . $e->getMessage();
            }
        }

        //datos para renderizar la pagina
        $baseMayorias = $em->getRepository('STIRBundle:BaseMayoria')->findAll();
        $tiposMayoria = $em->getRepository('STIRBundle:TipoMayoria')->findAll();
        $tiposResultados = $em->getRepository('STIRBundle:Resultado')->findAll();
        $tiposVotacion = $em->getRepository('STIRBundle:TipoVotacion')->findAll();
        $votosPosibles = $em->getRepository('STIRBundle:Voto')->findAll();
        $diputados = $em->getRepository('STIRBundle:Diputado')->findBy([], ['apellido' => 'ASC']);
        $bloques = $em->getRepository('STIRBundle:Bloque')->findAll();
        $provincias = $em->getRepository('STIRBundle:Provincia')->findAll();

        return $this->render('RAEBundle:Default:editar_acta.html.twig', Array("baseMayoria" => $baseMayorias,
            "tipoMayoria" => $tiposMayoria,
            "tipoResultado" => $tiposResultados,
            "tiposVotacion" => $tiposVotacion,
            "votosPosibles" => $votosPosibles,
            "diputadosActivos" => $diputados,
            "bloques" => $bloques,
            "provincias" => $provincias,
            "mensaje" => $mensaje,
            "error" => $error));
    }

    /**
     * @Route("/select/pendientes", name="_select_actas_pendientes")
     */
    public function actasPendientesAction()
    {

        $mensaje = null;
        $error = null;

        //datos para renderizar la pagina
        $actasPendientes = $this->get('helper')->getActasPendientes();

        return $this->render('RAEBundle:Default:actas_pendientes.html.twig', Array("actasPendientes" => $actasPendientes));
    }



    /**
     * @Route("/update/detalle", name="_update_detalle_actas_finalizadas")
     */
    public function updateDetalleExistenteAction()
    {

        $mensaje = null;
        $error = null;

        //datos para renderizar la pagina
        $actasConDetalle = $this->get('helper')->getActasConDetalle();

        return $this->render('RAEBundle:Default:update_acta_detalle.html.twig', Array("actasConDetalle" => $actasConDetalle));
    }


    /**
     * @Route("/edit/detalle/detalle/{id}/{order}", name="_select_actas_pendientes_detalle")
     * @param Request $request
     * @param $id
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function createDetalleActaAction(Request $request, $id, $order){

        $mensaje = null;
        $error = null;
        $em = $this->container->get('doctrine')->getManager();

        //datos para renderizar pagina y hacer chequeos
        $acta = $em->getRepository('STIRBundle:Acta')->find($id);
        if (!$acta) {
            die("No existe el acta");
        }
        $votosPosibles = $em->getRepository('STIRBundle:Voto')->findAll();
        $diputados = $this->get('helper')->getDiputadosNotInActa($acta);
        $bloques = $this->get('helper')->getBloquesSinIdsInvalidos();
        $provincias = $em->getRepository('STIRBundle:Provincia')->findAll();

        //Si el acta tiene detalle, devuelvo la vista con el detalle y no muestro el pop up
        if ($this->get('helper')->actaTieneDetalle($acta)) {
            if(sizeof($acta->getDetalleActas())!=257){
                //die("El detalle del acta es incorrecto: " . sizeof($acta->getDetalleActas()));
                //die("El detalle del acta tiene un tamaño incorrecto diferente a 257: tamanio actual -> " . sizeof($acta->getDetalleActas()));
                die("El detalle del acta tiene un tamanio incorrecto diferente a 257: tamanio actual -> " . sizeof($acta->getDetalleActas()));
            }
            return $this->render('RAEBundle:Default:actas_pendientes_detalle.html.twig', array("detalleActa" => $this->get('helper')->getDetalleOrdenado($acta, $order),
                "cabeceraActa" => $acta,
                "votos" => $votosPosibles,
                "diputados" => $diputados,
                "bloques" => $bloques,
                "provincias" => $provincias));
        }

        if ($request->getMethod() == 'POST') {

            try {
                $detalles = $this->get('helper')->getDetalleActaNueva($request,$acta);
            } catch (Exception $e) {
                return $this->render('RAEBundle:Default:actas_pendientes_detalle.html.twig', array("error" => $e->getMessage(),"popUp"=>1));
            }

            return $this->render('RAEBundle:Default:actas_pendientes_detalle.html.twig', array("detalleActa" => $detalles, "cabeceraActa" => $acta, "votos" => $votosPosibles, "diputados" => $diputados, "bloques" => $bloques, "provincias" => $provincias));
        }

        return $this->render('RAEBundle:Default:actas_pendientes_detalle.html.twig', array("popUp" => 1, 'detalleActas' => $acta->getDetalleActas()));

    }


    /**
     * @Route("/salir", name="exit")
     */
    public function exitAction()
    {
        $this->get('bos_user')->logout();
        return new RedirectResponse($this->generateUrl(($this->get('login_service')->getLogin())));
    }

    /**
     * @Route("/", name="home")
     */
    public function homeAction()
    {
        return $this->render('RAEBundle:Default:index.html.twig');
    }

}
